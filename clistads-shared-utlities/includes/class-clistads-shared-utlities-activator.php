<?php

/**
 * Fired during plugin activation
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_Shared_Utlities_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
