<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_Shared_Utlities_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
