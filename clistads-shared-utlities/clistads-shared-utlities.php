<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://clistads.com/
 * @since             1.0.0
 * @package           Clistads_Shared_Utlities
 *
 * @wordpress-plugin
 * Plugin Name:       Clistads Shared Utlities
 * Plugin URI:        https://clistads.com/
 * Description:       This plugin is required to run any other clistads plugins developed by Dongyang Chen
 * Version:           1.0.0
 * Author:            Dongyang Chen
 * Author URI:        https://clistads.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       clistads-shared-utlities
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CLISTADS_SHARED_UTLITIES_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-clistads-shared-utlities-activator.php
 */
function activate_clistads_shared_utlities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-clistads-shared-utlities-activator.php';
	Clistads_Shared_Utlities_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-clistads-shared-utlities-deactivator.php
 */
function deactivate_clistads_shared_utlities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-clistads-shared-utlities-deactivator.php';
	Clistads_Shared_Utlities_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_clistads_shared_utlities' );
register_deactivation_hook( __FILE__, 'deactivate_clistads_shared_utlities' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-clistads-shared-utlities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_clistads_shared_utlities() {

	$plugin = new Clistads_Shared_Utlities();
	$plugin->run();

}
run_clistads_shared_utlities();
