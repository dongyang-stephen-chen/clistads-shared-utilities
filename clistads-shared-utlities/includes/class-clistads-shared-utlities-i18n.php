<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://clistads.com/
 * @since      1.0.0
 *
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Clistads_Shared_Utlities
 * @subpackage Clistads_Shared_Utlities/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_Shared_Utlities_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'clistads-shared-utlities',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
